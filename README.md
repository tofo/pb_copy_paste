
# pbcopy and pbpaste

pbcopy and pbpaste are simple command-line tools for interacting with the system clipboard in Unix-like operating systems, such as macOS and Linux.

## Installation

You can install pbcopy and pbpaste by compiling the Rust source code provided

## Usage

```sh
# pbcopy reads from standard input and copies the content to the system clipboard.
# Example usage: Copy the contents of a file to the clipboard
cat file.txt | pbcopy
# Example usage: Copy the output of a command to the clipboard
ls | pbcopy

# pbpaste retrieves the content from the system clipboard and prints it to standard output.
# Example usage: Paste the content from the clipboard into a file
pbpaste > file.txt
# Example usage: Paste the content from the clipboard and display it
pbpaste | less
```

## Use Cases
  
- **Piping Data:** You can use pbcopy in conjunction with other command-line utilities by piping data into it. For example, you can pipe the output of a command directly into pbcopy to

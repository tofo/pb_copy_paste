mod move_mouse_figure_eight;
mod move_mouse_hypotrochoid;
mod move_mouse_lissajous;
mod resource;

use resource::Resource;
use std::time::Duration;

fn main() {
  let resource = Resource::new();
  move_mouse_figure_eight::move_mouse_figure_eight(&resource, Duration::from_secs(10));
  move_mouse_hypotrochoid::move_mouse_hypotrochoid(&resource, Duration::from_secs(10));
  move_mouse_lissajous::move_mouse_lissajous(&resource, Duration::from_secs(10));
}

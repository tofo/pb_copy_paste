use crate::resource::Resource;
use core_graphics::event::CGEventTapLocation;
use std::thread;
use std::time::Duration;

pub fn move_mouse_figure_eight(resource: &Resource, duration: Duration) {
  let start_time = std::time::Instant::now();

  while start_time.elapsed() < duration {
    for theta in (0..=360).step_by(1) {
      let angle_rad = theta as f64 * std::f64::consts::PI / 180.0;

      let x = resource.center_x + resource.radius * (2.0 * angle_rad).sin();
      let y =
        resource.center_y + resource.radius * (2.0 * angle_rad).sin() * (2.0 * angle_rad).cos();

      let move_event = resource.create_move_event(x, y); // Using create_move_event method

      move_event.post(CGEventTapLocation::HID);
      thread::sleep(Duration::from_millis(20));
    }
  }
}

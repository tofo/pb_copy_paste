use crate::resource::Resource;
use core_graphics::event::CGEventTapLocation;
use std::thread;
use std::time::{Duration, Instant}; // Assuming Resource struct is defined in another module

pub fn move_mouse_hypotrochoid(resource: &Resource, duration: Duration) {
  let start_time = Instant::now();

  while start_time.elapsed() < duration {
    // Get the size of the main screen
    let screen_bounds = resource.screen_bounds;
    let (max_x, max_y) = (
      screen_bounds.size.width as f64,
      screen_bounds.size.height as f64,
    );

    // Calculate maximum values for a and b to ensure the pointer does not hit the edges of the screen
    let max_a = max_x * 0.6; // 60% of the screen width
    let max_b = max_y * 0.6; // 60% of the screen height

    // Define the parameters for the Hypotrochoid curve
    let a = max_a / 2.0;
    let b = max_b / 2.0;
    let c = 0.3 * (max_a - max_b); // Choose a suitable value for c (e.g., 30% of the difference between a and b)

    // Calculate the center of the screen
    let center_x = max_x / 2.0;
    let center_y = max_y / 2.0;

    // Increase the number of iterations to make the movement last longer
    let num_iterations = 720;

    // Generate points along the Hypotrochoid curve
    for theta in (0..=num_iterations).step_by(1) {
      let angle_rad = theta as f64 * std::f64::consts::PI / 180.0;

      // Calculate x and y coordinates for the Hypotrochoid curve
      let x = (a - b) * angle_rad.cos() + c * ((a / b - 1.0) * angle_rad).cos();
      let y = (a - b) * angle_rad.sin() - c * ((a / b - 1.0) * angle_rad).sin();

      // Calculate the scaled and centered coordinates
      let scaled_x = (x / max_a) * (max_x / 2.0) + center_x;
      let scaled_y = (y / max_b) * (max_y / 2.0) + center_y;

      // Create a mouse move event and post it
      let move_event = resource.create_move_event(scaled_x, scaled_y);
      move_event.post(CGEventTapLocation::HID);

      // Adjust the sleep duration for smoother movement
      thread::sleep(Duration::from_millis(10)); // Adjust the sleep duration for smoother movement
    }
  }
}

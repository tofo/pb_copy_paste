use crate::resource::Resource;
use core_graphics::event::CGEventTapLocation;
use std::thread;
use std::time::{Duration, Instant}; // Assuming Resource struct is defined in another module

pub fn move_mouse_lissajous(resource: &Resource, duration: Duration) {
  let start_time = Instant::now();

  while start_time.elapsed() < duration {
    // Get the size of the main screen
    let screen_bounds = resource.screen_bounds;
    let (max_x, max_y) = (
      screen_bounds.size.width as f64,
      screen_bounds.size.height as f64,
    );

    // Define the parameters for Lissajous curves
    let n = 3.0;
    let c = std::f64::consts::PI / 4.0; // Phase difference

    // Generate points along the Lissajous curves
    for theta in (0..=360).step_by(1) {
      let angle_rad = theta as f64 * std::f64::consts::PI / 180.0;

      // Calculate x and y coordinates for the Lissajous curves
      let x = (max_x / 2.0) * (angle_rad * n + c).sin();
      let y = (max_y / 2.0) * angle_rad.sin();

      // Calculate the scaled and centered coordinates
      let scaled_x = x + resource.center_x;
      let scaled_y = y + resource.center_y;

      // Create a mouse move event and post it
      let move_event = resource.create_move_event(scaled_x, scaled_y);

      move_event.post(CGEventTapLocation::HID);

      // Sleep for a short duration to control the speed of movement
      thread::sleep(Duration::from_millis(20)); // Adjust the sleep duration for smoother movement
    }
  }
}

use clipboard::{ClipboardContext, ClipboardProvider};
use std::io::{self, Read};

fn main() {
  let mut buffer = String::new();

  if let Err(e) = io::stdin().read_to_string(&mut buffer) {
    eprintln!("Failed to read from stdin: {}", e);
    return;
  }

  let mut ctx: ClipboardContext = ClipboardProvider::new().unwrap();
  if let Err(e) = ctx.set_contents(buffer) {
    eprintln!("Failed to set clipboard contents: {}", e);
    return;
  }
}

use clipboard::{ClipboardContext, ClipboardProvider};

fn main() {
  let mut ctx: ClipboardContext = ClipboardProvider::new().unwrap();

  let clipboard_content = match ctx.get_contents() {
    Ok(content) => content,
    Err(_) => {
      eprintln!("Failed to read from clipboard");
      return;
    }
  };

  print!("{}", clipboard_content);
}

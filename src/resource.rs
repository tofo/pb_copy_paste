use core_graphics::display;
use core_graphics::event::{CGEvent, CGEventType, CGMouseButton};
use core_graphics::event_source::CGEventSource;
use core_graphics::event_source::CGEventSourceStateID;
use core_graphics::geometry::{CGPoint, CGRect}; // Importing CGPoint from core_graphics::geometry
use rand::rngs::ThreadRng;
use rand::thread_rng;

pub struct Resource {
  pub event_source: CGEventSource,
  pub main_screen: u32,
  pub screen_bounds: CGRect,
  pub max_x: f64,
  pub max_y: f64,
  pub center_x: f64,
  pub center_y: f64,
  pub rng: ThreadRng,
  pub radius: f64,
  pub last_move_event: Option<CGEvent>,
}

impl Resource {
  pub fn new() -> Self {
    let event_source = CGEventSource::new(CGEventSourceStateID::HIDSystemState).unwrap();

    let main_screen = unsafe { display::CGMainDisplayID() };
    let screen_bounds = unsafe { display::CGDisplayBounds(main_screen) };
    let max_x = screen_bounds.size.width as f64;
    let max_y = screen_bounds.size.height as f64;
    let center_x = max_x / 2.0;
    let center_y = max_y / 2.0;
    let radius = (max_x.min(max_y) / 3.0) as f64;
    let rng = thread_rng();

    Resource {
      event_source,
      main_screen,
      screen_bounds,
      max_x,
      max_y,
      center_x,
      center_y,
      rng,
      radius,
      last_move_event: None,
    }
  }

  pub fn create_move_event(&self, x: f64, y: f64) -> CGEvent {
    CGEvent::new_mouse_event(
      self.event_source.clone(),
      CGEventType::MouseMoved,
      CGPoint::new(x, y), // Using CGPoint from core_graphics::geometry
      CGMouseButton::Left,
    )
    .unwrap()
  }
}
